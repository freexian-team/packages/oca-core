oca-core (11.0.20191007-4) unstable; urgency=medium

  [ Sébastien Delafond ]
  * Source-only upload (Closes: #964448)

 -- Sebastien Delafond <seb@debian.org>  Tue, 07 Jul 2020 15:38:01 +0200

oca-core (11.0.20191007-3) unstable; urgency=medium

  [ Sébastien Delafond ]
  * Link one more font from glyphicons
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Wed, 24 Jun 2020 10:43:13 +0200

oca-core (11.0.20191007-2) unstable; urgency=medium

  [ Sébastien Delafond ]
  * Replace dependency on fonts-glewlwyd with awesome+glyphicons (Closes: 962877)

 -- Sebastien Delafond <seb@debian.org>  Wed, 24 Jun 2020 09:31:04 +0200

oca-core (11.0.20191007-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Sébastien Delafond ]
  * d/gbp.conf
  * New upstream version 11.0.20191007
  * d/py3dist-overrides: add suds_jurko definition

 -- Sebastien Delafond <seb@debian.org>  Mon, 04 May 2020 10:18:36 +0200

oca-core (11.0.20180730-1) unstable; urgency=medium

  [ Sophie Brun ]
  * New upstream version 11.0.20180730 compatible with Python3.7
    (Closes: #904370)
  * Fix VCS fields
  * Bump Standards-Version to 4.1.5 (no changes)
  * Remove unused lintian-overrides

  [ Raphaël Hertzog ]
  * Update watch file to track release tarballs from OCB instead of Odoo

 -- Sophie Brun <sophie@freexian.com>  Fri, 24 Aug 2018 18:35:22 +0200

oca-core (11.0.20180420-1) unstable; urgency=medium

  [ Sophie Brun ]
  * Initial packaging of OCA in Debian (Closes: #896157)

  [ Raphaël Hertzog ]
  * Add wkhtmltopdf to Recommends.
  * Conflicts with "odoo" to not allow co-installation since they share the
    same Python namespace.
  * Rename odoo.service into oca-core.service to get it installed by default.
  * Add README.Debian with basic installation instructions.
  * Add a basic autopkgtest.
  * Update maintainer field to use new team and put myself in Uploaders.
  * Update Standards-Version to 4.1.4.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 20 Apr 2018 16:26:18 +0200
